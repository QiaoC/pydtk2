from .dtk2mapper import DTK2MoabManager, DTK2Mapper
from ._version import __version__

WENLAND0 = [0, 0]
WENLAND2 = [0, 2]
WENLAND4 = [0, 4]
WENLAND6 = [0, 6]
WU0 = [1, 0]
WU2 = [1, 2]
WU4 = [1, 4]
BUHMANN3 = [2, 3]
